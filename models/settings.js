// Settings module that other files can get settings from
var config = require('./config');
// console.log(config.server);
// Load Server Credentials

// module.exports.ENVIRONMENT = process.env.NODE_ENV;

const DATABASE_HOST = module.exports.DATABASE_HOST = config.server.databaseHost;
const DATABASE_PORT = module.exports.DATABASE_PORT = config.server.databasePort;
const DATABASE_USER = module.exports.DATABASE_USER = config.server.databaseUser;
const DATABASE_PASSWORD = module.exports.DATABASE_PASSWORD = config.server.databasePassword;
const DATABASE_NAME = module.exports.DATABASE_NAME = config.server.databaseName;

module.exports.DATABASE_CONNECTION_STRING = 'mongodb://' + DATABASE_USER + ':' + DATABASE_PASSWORD + '@' + DATABASE_HOST + ':' + DATABASE_PORT + '/' + DATABASE_NAME

// const TRACK_TOKEN = module.exports.TRACK_TOKEN = config.get('track.token');
// const TRACK_PROD_TOKEN = module.exports.TRACK_PROD_TOKEN = config.get('track.prod_token');

const BOT_TOKEN = module.exports.BOT_TOKEN = config.bot.token;
const TEST_TOKEN = module.exports.TEST_TOKEN = config.bot.test_token;
const TWILIO_SID = module.exports.TWILIO_SID = config.twilio.account_sid;
const TWILIO_AUTH = module.exports.TWILIO_AUTH = config.twilio.auth_token;


// const API_URL = module.exports.API_URL = config.server.staging_url;
const API_URL = module.exports.API_URL = config.server.production_url;

// const BADGE_URL = module.exports.BADGE_URL = config.get('server.badge_url');
// const BOT_AUTH_TOKEN = module.exports.BOT_AUTH_TOKEN = config.get('bot.auth_token');
// const BASE_URL = module.exports.BASE_URL = config.get('server.base_url');
