const mongoUserRepo = require('./mongoRepository')('users');
const cryptoUtils = require('../common/cryptoUtils');

/**
 * Retrieves user from Mongo and decrypts the PK
 * @param selector
 * @returns {Promise.<*>}
 */
exports.findOneWithDecryptedPK = async function findOneWithDecryptedKeyPair(selector) {
    let userFromMongo = await mongoUserRepo.findOne(selector);
    let decryptedPK = cryptoUtils.aesDecrypt(userFromMongo.keyPair.privateKey);
    userFromMongo.keyPair.privateKey = decryptedPK;
    return userFromMongo;
};

/**
 * Encrypts user PK and saves it in Mongo
 * @param user
 * @returns {Promise.<void>}
 */
exports.encryptPKAndInsert = async function encryptPKAndInsert(user) {
    let encryptedPK = cryptoUtils.aesEncrypt(user.keyPair.privateKey);
    user.keyPair.privateKey = encryptedPK;
    await mongoUserRepo.insert(user);
};