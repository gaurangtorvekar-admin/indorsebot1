const request = require('request');
const settings = require('../../settings');

/**
 *
 * @param bufferedData
 * @returns {Promise.<void>}
 */
exports.uploadFile = async function uploadFile(bufferedData) {
    if (uploadsAreEnabled()) {
        let options = {
            method: 'POST',
            url: 'https://ipfs.infura.io:5001/api/v0/add',
            headers: {
                'cache-control': 'no-cache',
                'content-type': 'multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW'
            },
            formData: {
                '': {
                    value: bufferedData,
                    options: {
                        filename: 'UET.png',
                        contentType: null
                    }
                }
            }
        };

        let requestResult = await new Promise((resolve, reject) => {
            request(options, function (error, response, body) {
                if (!error) {
                    resolve(JSON.parse(body));
                } else {
                    reject(error);
                }
            })
        });
        return requestResult.Hash
    }
};

function uploadsAreEnabled() {
    return settings.ENVIRONMENT === 'staging' || settings.ENVIRONMENT === 'production' || settings.ENVIRONMENT === 'test_tmp';
}