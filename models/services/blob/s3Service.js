const settings = require('../../settings');
const config = require('config');

let s3, s3Bucket;

if (uploadsAreEnabled()) {
    const AWS = require('aws-sdk');
    AWS.config.loadFromPath('./config/aws.json');
    s3 = new AWS.S3();
    s3Bucket = new AWS.S3({params: {Bucket: config.get('aws.s3Bucket')}});
}

/**
 *
 * @param bufferedData
 * @returns {Promise.<void>}
 */
exports.uploadFile = async function uploadFile(key, bufferedData) {
    if (uploadsAreEnabled()) {

        let data = {
            Key: key,
            Body: bufferedData,
            ContentEncoding: 'base64',
            ContentType: 'image/jpeg'
        };

        await new Promise((resolve, reject) => {
            s3Bucket.putObject(data, function (error, result) {
                if (!error) {
                    resolve(result);
                } else {
                    reject(error);
                }
            })
        });
    }
};

function uploadsAreEnabled() {
    return settings.ENVIRONMENT === 'staging' || settings.ENVIRONMENT === 'production' || settings.ENVIRONMENT === 'test_tmp';
}
