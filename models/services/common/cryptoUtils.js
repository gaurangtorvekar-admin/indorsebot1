let _self = this;
const crypto = require('crypto');
const randtoken = require('rand-token');
const config = require('config');
const jwt = require('jsonwebtoken');
const ENCRYPTION_KEY = config.get('crypto.aesPk');
const IV_LENGTH = 16;
const safeObjects = require('./safeObjects');

exports.genRandomString = function genRandomString(length) {
    return crypto.randomBytes(Math.ceil(length / 2))
        .toString('hex') /** convert to hexadecimal format */
        .slice(0, length);
    /** return required number of characters */
};

exports.sha512 = function sha512(password, salt) {
    let hash = crypto.createHmac('sha512', salt);
    /** Hashing algorithm sha512 */
    hash.update(password);
    let value = hash.digest('hex');
    return {
        salt: salt,
        passwordHash: value
    };
};

exports.sha256 = function sha256(password, salt) {
    let hash = crypto.createHmac('sha256', salt);
    /** Hashing algorithm sha256 */
    hash.update(password);
    let value = hash.digest('hex');
    return {
        salt: salt,
        passwordHash: value
    };
};

exports.saltHashPassword = function saltHashPassword(userpassword) {
    let salt = _self.genRandomString(16);
    /** Gives us salt of length 16 */
    return _self.sha512(userpassword, salt);
};

exports.generateItemKey = function generateItemKey() {
    return randtoken.generate(16) + Math.floor(Date.now() / 1000).toString();
};

exports.generateOTP = function generateOTP() {
    return randtoken.generate(5);
};

exports.aesEncrypt = function aesEncrypt(text) {
    let iv = crypto.randomBytes(IV_LENGTH);
    let cipher = crypto.createCipheriv('aes-256-cbc', new Buffer(ENCRYPTION_KEY), iv);
    let encrypted = cipher.update(text);

    encrypted = Buffer.concat([encrypted, cipher.final()]);

    return iv.toString('hex') + ':' + encrypted.toString('hex');
};

exports.aesDecrypt = function aesDecrypt(text) {
    let textParts = text.split(':');
    let iv = new Buffer(textParts.shift(), 'hex');
    let encryptedText = new Buffer(textParts.join(':'), 'hex');
    let decipher = crypto.createDecipheriv('aes-256-cbc', new Buffer(ENCRYPTION_KEY), iv);
    let decrypted = decipher.update(encryptedText);

    decrypted = Buffer.concat([decrypted, decipher.final()]);

    return decrypted.toString();
};

exports.generateJWT = function generateJWT(user) {
    return jwt.sign(safeObjects.safeJWTObject(user), config.get('JWT.jwtSecret'), {
        expiresIn: 60 * 60 * 24 * 31 // expires in 31 days
    });
};