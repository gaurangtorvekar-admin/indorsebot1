const sanitize = require('mongo-sanitize');
const xssSanitize = require('sanitizer');

const SAFE_USER_FIELDS = ['name', 'bio', 'ethaddress', 'user_id', 'username', 'email', 'user_id', 'work', 'education', 'is_pass', '_id'
    , `img_url`, `photo_ipfs`, `score_count`, `role`, `verified`, `approved`, 'last_linkedin_pdf_import_at', 'is_airbitz_user', 'language'
,'isFacebookLinked','isGoogleLinked','isLinkedInLinked','isAirbitzLinked','skills','skill_name'];

const SAFE_COMPANY_FIELDS = ['admin', 'company_name', 'cover_data', 'created_by_admin', 'description', 'item_key',
    'last_updated_by', 'last_updated_timestamp', 'logo_data', 'pretty_id', 'social_links'];

const SAFE_JWT_FIELDS = ['name', 'ethaddress', 'user_id', 'username', 'email', 'user_id','role'];

function extractFields(obj, fieldNames) {
    let extractedFields = {};

    fieldNames.forEach((fieldName) => {
        let fieldValue = obj[fieldName];
        if (fieldValue) {
            extractedFields[fieldName] = recursivelySanitize(fieldValue);
        }
    });

    return extractedFields;
}

exports.safeObjectParse = function safeObjectParse(obj, fieldNames) {
    return extractFields(obj, fieldNames)
};

/**
 * Safely extracts body fields into a new object.
 * Converts all case insensitive fields to lower case.
 * @param req
 * @para fieldNames array of strings
 */
exports.safeReqestBodyParser = function safeReqestBodyParser(req, fieldNames) {
    return safelyParseObject(req.body, fieldNames);
};

/**
 * Safely extracts body fields into a new object.
 * Converts all case insensitive fields to lower case.
 * @param req
 * @para fieldNames array of strings
 */
exports.safeReqestQueryParser = function safeReqestQueryParser(req, fieldNames) {
    return safelyParseObject(req.query, fieldNames);
};

exports.safeUserObject = function safeUserObject(user) {
    return extractFields(user, SAFE_USER_FIELDS)
};

exports.safeCompanyObject = function safeUserObject(company) {
    return extractFields(company, SAFE_COMPANY_FIELDS)
};

exports.safeJWTObject = function safeJWTObject(user) {
    return extractFields(user, SAFE_JWT_FIELDS )
};

function safelyParseObject(object, fieldNames) {
    let safeRequest = extractFields(object, fieldNames);

    if (safeRequest.email) {
        safeRequest.email = safeRequest.email.toLowerCase();
    }

    if (safeRequest.username) {
        safeRequest.username = safeRequest.username.toLowerCase();
    }

    return safeRequest
}

function recursivelySanitize(field) {
    if (field instanceof Object) {
        sanitize(field);
        for (var key in field) {
            field[key] = recursivelySanitize(field[key]);
        }
        return field;
    } else {
        if (field instanceof String) {
            return xssSanitize.sanitize(field);
        } else {
            return field;
        }
    }
}

exports.sanitize = recursivelySanitize;
