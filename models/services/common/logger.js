const winston = require('winston');

exports.getLogger = function getLogger(label) {

    return new (winston.Logger)({
        transports: [
            new (winston.transports.Console)({
                level: 'error',
                timestamp: true,
                colorize: true,
                prettyPrint: true,
            })
        ]
    })
};

exports.getBigchainLogger = function getBigchainLogger() {
    return new (winston.Logger)({
        transports: [
            new (winston.transports.File)({
                filename: 'bigchain.log',
                level: 'debug',
                prettyPrint: true,
                json: false
            })
        ]
    })
};