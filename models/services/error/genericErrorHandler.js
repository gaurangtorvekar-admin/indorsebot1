const logger = require('../common/logger').getLogger('ErrorMiddleware');
const uuidv1 = require('uuid/v1');

exports.handleError = function handleError(err, req, res, next) {

    const requestID = uuidv1();

    logger.error(err.stack, {
        url: req.url,
        method: req.method,
        requestID: requestID,
        body: req.body
    });

    const responseData = {
        message: err.message,
        requestID: requestID,
        success: false
    };

    res.status(err.code || 500);
    res.send(responseData);
};
