const TelegramBot = require('node-telegram-bot-api');
var mongo = require('mongodb');
var settings = require('./models/settings.js');
var validate = require("validate.js");
var random = require('math-random');
var twilio = require('twilio');

// Token for IndorseBot
const token = settings.BOT_TOKEN;
// Token for gaurang_test_bot
// const token = settings.TEST_TOKEN;

const bot = new TelegramBot(token, { polling: true });

const request = require("request");

var current_user_info = {};
var current_signup_info = {};
var re_email = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
var API_URL = settings.API_URL;

var accountSid = settings.TWILIO_SID; // Your Account SID from www.twilio.com/console
var authToken = settings.TWILIO_AUTH; // Your Auth Token from www.twilio.com/console

var twilio_client = new twilio(accountSid, authToken);

var list_invite_codes = {
    "ind123": "+6591452167",
    "ind124": "+6598759785",
    "ind125": "",
    "ind126": ""
}

const MongoClient = mongo.MongoClient;
var db;

MongoClient.connect(settings.DATABASE_CONNECTION_STRING, function(err, client) {
    if (err) return console.log(err);
    db = client.db('dreambot_production_backup');
    console.log("DB is connected");
});

bot.on('polling_error', (error) => {
    console.log(error); // => 'EFATAL'
});

bot.on('message', (msg) => {
    // Various commands to listen to...
    var Hi = "hi";
    var signup = "Sign Up";
    var createClaim = "Create a claim";
    var skill_python = "Python";
    var skill_js = "JavaScript";
    var skill_python = "Python";
    var skills_description = "Save the description!"
    var skills_link = "Save the link!";
    var skills_email = "Save the email!"
    var bye = "bye";
    var finish = "Finish";

    if (msg.text.toString().toLowerCase().indexOf(Hi) == 0) {
        bot.sendMessage(msg.chat.id, "Hello " + msg.from.first_name);
    } else if (msg.text.toString().toLowerCase().includes(bye)) {
        bot.sendMessage(msg.chat.id, "Hope to see you around again!");
    } else if (msg.text.indexOf(signup) == 0) {
        current_signup_info[msg.from.id] = { "status": "signup_adding_username" };
        console.log(current_signup_info);
        bot.sendMessage(msg.chat.id, "Great! Let's get you started. To begin with, let's try to see if you can find a unique username for yourself!\n\n<b>Please enter a username</b> you would like to book on Indorse, so that you can get a profile like this - https://indorse.io/indorsebot", {
            "reply_markup": {
                "hide_keyboard": true
            },
            parse_mode: "HTML"
        });
    } else if (msg.text.indexOf(createClaim) == 0) {
        bot.sendMessage(msg.chat.id, "Do you have an invite code? \n\nYou can always start over by clicking here - /start", {
            "reply_markup": {
                "inline_keyboard": [
                    [
                        { text: 'Yes', callback_data: 'invite_code_callback' },
                        { text: 'No', callback_data: 'no_invite_code_callback' },
                    ]
                ],
                "hide_keyboard": true
            }
        });
    } else if (msg.text.indexOf(finish) == 0) {
        bot.sendMessage(msg.chat.id, "Thank you for creating a claim, we have sent a link to your email address. If you have any questions, you can get in touch with us at help@indorse.io. \n\nYou can always start over by clicking here - /start", {
            "reply_markup": {
                "hide_keyboard": true
            }
        });
    } else if (current_user_info[msg.from.id]) {
        // ===== State machine for Signup flow
        if (current_signup_info[msg.from.id]) {
            if (current_signup_info[msg.from.id].status) {
                if (current_signup_info[msg.from.id].status.includes("signup_adding_username")) {
                    var potential_username = msg.text;
                    console.log(potential_username);
                    var request_url = API_URL + '/findusername';
                    var options = {
                        method: 'POST',
                        url: request_url,
                        headers: {
                            'cache-control': 'no-cache',
                            'content-type': 'application/json'
                        },
                        body: { username: potential_username },
                        json: true
                    };

                    request(options, function(error, response, body) {
                        if (error) {
                            console.log(error);
                        } else {
                            if (body.success) {
                                current_signup_info[msg.from.id].signup_username = potential_username;
                                console.log(current_signup_info);
                                console.log("In the signup username callback");
                                current_signup_info[msg.from.id].status = "signup_adding_name";
                                console.log(current_signup_info);
                                bot.sendMessage(msg.chat.id, "<b>Please enter your name</b>. \nYou can always start over by clicking here - /start", { parse_mode: "HTML" });
                            } else {
                                bot.sendMessage(msg.chat.id, "<b>Please add another username</b>. This one is already taken up! \nYou can always start over by clicking here - /start", { parse_mode: "HTML" });
                            }
                        }
                    });
                } else if (current_signup_info[msg.from.id].status.includes("signup_adding_name")) {
                    current_signup_info[msg.from.id].signup_name = msg.text;
                    current_signup_info[msg.from.id].status = "signup_adding_email";
                    console.log(current_signup_info);
                    bot.sendMessage(msg.chat.id, "<b>Please enter your email address</b>. \n\nYou can always start over by clicking here - /start", { parse_mode: "HTML" });
                } else if (current_signup_info[msg.from.id].status.includes("signup_adding_email")) {
                    if (re_email.test(msg.text.toString().toLowerCase())) {
                        current_signup_info[msg.from.id].signup_email = msg.text;
                        console.log(current_signup_info);
                        var request_url = API_URL + '/signup';
                        var options = {
                            method: 'POST',
                            url: request_url,
                            headers: {
                                'cache-control': 'no-cache',
                                'content-type': 'application/json'
                            },
                            body: {
                                username: current_signup_info[msg.from.id].signup_username,
                                name: current_signup_info[msg.from.id].signup_name,
                                email: current_signup_info[msg.from.id].signup_email,
                                password: random().toString(),
                                source: "bot"
                            },
                            json: true
                        };

                        request(options, function(error, response, body) {
                            if (error) {
                                console.log("Got an error from API - - - -");
                                console.log(error);
                            } else {
                                console.log("Got back a response from the API - - - - - ");
                                console.log(response.body);
                                if (response.body.message == "User with this email address or username exists") {
                                    bot.sendMessage(msg.chat.id, "It appears that a user with this email address already exists. Please try again. \nYou can always start over by clicking here - /start");
                                    current_signup_info[msg.from.id] = {};
                                } else if (response.body.message == "Verification email sent successfully") {
                                    bot.sendMessage(msg.chat.id, "Great! We have sent you an email with verification link and further instructions!\nWelcome to Indorse!\n\n<b>IMPORTANT - We have set up a temporary password for your email. You need to click 'Forgot Password' before logging in for the first time</b>. \nYou can always start over by clicking here - /start", { parse_mode: "HTML" });
                                    current_signup_info[msg.from.id] = {};
                                }
                            }
                        });
                    } else {
                        console.log("Valid email from signup flow --- ")
                        bot.sendMessage(msg.chat.id, "Please enter a valid email");
                    }
                }
            }
        }

        // ===== State machine (for the CLAIMS process) to capture various status!!
        if (current_user_info[msg.from.id].status) {
            if (current_user_info[msg.from.id].status.includes("adding_description")) {
                current_user_info[msg.from.id].description = msg.text;
                current_user_info[msg.from.id].status = "adding_level";
                console.log(current_user_info);
                bot.sendMessage(msg.chat.id, "Now <b>select a level</b> of expertise! \nYou can always start over by clicking here - /start", {
                    "parse_mode": "HTML",
                    "reply_markup": {
                        "inline_keyboard": [
                            [
                                { text: 'Beginner', callback_data: 'beginner_level' },
                                { text: 'Intermediate', callback_data: 'intermediate_level' },
                                { text: 'Expert', callback_data: 'expert_level' }
                            ]
                        ]
                    },
                    "one_time_keyboard": true
                });
            } else if (current_user_info[msg.from.id].status.includes("adding_link")) {
                var link = msg.text;
                if (!link.includes("https://")) {
                    console.log("Adding https");
                    link = "https://" + link;
                }
                if (!validate({ website: link }, { website: { url: true } })) {
                    current_user_info[msg.from.id].repo_link = link;
                    current_user_info[msg.from.id].status = "adding_email"
                    console.log(current_user_info);
                    bot.sendMessage(msg.chat.id, "Now you need to <b>add your email address</b>. (Please make sure it's a valid email address). \nYou can always start over by clicking here - /start", { parse_mode: "HTML" });
                } else {
                    bot.sendMessage(msg.chat.id, "Please enter a valid URL. \nYou can always start over by clicking here - /start");
                }
            } else if (current_user_info[msg.from.id].status.includes("adding_email")) {
                if (re_email.test(msg.text.toString().toLowerCase())) {
                    current_user_info[msg.from.id].claim_email = msg.text;
                    current_user_info[msg.from.id].status = "verifying_email"

                    // Creating 6-digit random number which would act as a verification code
                    var one_time_code = Math.floor(100000 + Math.random() * 900000);

                    if (current_user_info[msg.from.id].invite_code) {
                        bot.sendMessage(msg.chat.id, "Great! We have sent you a link on your email to <b>verify the claim</b>.\n\n<b>Good news!</b> Now you can claim your free drink at the counter by showing this code to the cashier - \n<code>" + one_time_code + "</code>\n\nYou can always start over by clicking here - /start", {
                            "reply_markup": {
                                "keyboard": [
                                    ["Finish"],
                                ]
                            },
                            parse_mode: "HTML",
                            "one_time_keyboard": true
                        });
                    } else {
                        bot.sendMessage(msg.chat.id, "Great! We have sent you a link on your email to <b>verify the claim</b>.\n\nYou can always start over by clicking here - /start", {
                            "reply_markup": {
                                "keyboard": [
                                    ["Finish"],
                                ]
                            },
                            parse_mode: "HTML",
                            "one_time_keyboard": true
                        });
                    }

                    var request_url = API_URL + '/graphql';
                    console.log(request_url);
                    var options = {
                        method: 'POST',
                        url: request_url,
                        headers: {
                            'cache-control': 'no-cache',
                            'content-type': 'application/json'
                        },
                        body: {
                            operationName: 'mutateCreateClaim',
                            variables: {
                                email: current_user_info[msg.from.id].claim_email,
                                title: 'Javascript',
                                desc: current_user_info[msg.from.id].description,
                                proof: current_user_info[msg.from.id].repo_link,
                                level: current_user_info[msg.from.id].level,
                                source: "bot"
                            },
                            query: 'mutation mutateCreateClaim($email: EmailAddress!, $title: String!, $desc: String!, $proof: URL, $level: ClaimLevel!, $source: String) {\n  createClaimDraft(email: $email, title: $title, desc: $desc, proof: $proof, level: $level, source: $source) {\n    userExists\n    __typename\n  }\n}\n'
                        },
                        json: true
                    };

                    request(options, function(error, response, body) {
                        if (error) {
                            console.log(error);
                            bot.sendMessage(msg.chat.id, "There was some problem with your information!\n\n \nYou can always start over by clicking here - /start", {
                                "reply_markup": {
                                    "keyboard": [
                                        ["/Start"],
                                    ]
                                },
                                "one_time_keyboard": true
                            });
                        } else {
                            var entered_invite_code = current_user_info[msg.from.id].invite_code;
                            if (list_invite_codes[entered_invite_code]) {
                                var phone_number = list_invite_codes[entered_invite_code];
                                console.log("Now sending a SMS to the claimant and the restaurant");
                                twilio_client.messages.create({
                                    body: 'Hello from Indorse! One more drink has been claimed by ' + msg.from.first_name + '. Please verify that the claimant shows you this code on his mobile - \n' + one_time_code,
                                    to: phone_number, // Text this number
                                    from: '+19842056351' // From a valid Twilio number
                                }).then((message) => console.log(message.sid));
                            }
                            current_user_info[msg.from.id] = {};
                        }
                        console.log(body);
                    });
                } else {
                    bot.sendMessage(msg.chat.id, "Please enter a valid email. \nYou can always start over by clicking here - /start");
                }
            } else if (current_user_info[msg.from.id].status.includes("adding_invite_code")) {
                current_user_info[msg.from.id].status = "";
                console.log(current_user_info);
                if (db) {
                    db.collection('bot_users', function(err, users_collection) {
                        users_collection.findOne({
                            $and: [
                                { 'telegram_id': msg.from.id },
                                { 'invite_code': msg.text }
                            ]
                        }, function(err, item) {
                            if (item) {
                                // User has already claimed his free drink
                                console.log(item);
                                console.log("This person has already claimed the invite code");
                                bot.sendMessage(msg.chat.id, "Sorry! You seem to have already claimed this invite code once.\n\n But you can validate your claim and earn up to 50 Indorse tokens.\nPlease select one of the skills below. \n\nYou can always start over by clicking here - /start", {
                                    "reply_markup": {
                                        "inline_keyboard": [
                                            [
                                                { text: 'JavaScript', callback_data: 'JavaScript' },
                                                { text: 'Java', callback_data: 'Java' },
                                                { text: 'C#', callback_data: 'C_sharp' },
                                                { text: 'Python', callback_data: 'Python' },
                                                { text: 'Solidity', callback_data: 'Solidity' }
                                            ]
                                        ],
                                        "hide_keyboard": true
                                    }
                                });
                            } else {
                                console.log(item);
                                console.log("This person has not claimed the invite code");
                                current_user_info[msg.from.id].invite_code = msg.text;
                                console.log(current_user_info);
                                users_collection.update({
                                    'telegram_id': msg.from.id
                                }, { '$set': { 'chat_id': msg.from.id, 'first_name': msg.from.first_name, 'last_name': msg.from.last_name, 'username': msg.from.username, 'invite_code': msg.text } }, {
                                    safe: true
                                }, function(err, result) {
                                    if (err) {
                                        console.log("Error while saving in the DB", err);
                                    } else {
                                        console.log("successfully saved invite code in the db");
                                        bot.sendMessage(msg.chat.id, "You can validate your claim and earn up to 50 Indorse tokens.\nPlease select one of the skills below. \n\nYou can always start over by clicking here - /start", {
                                            "reply_markup": {
                                                "inline_keyboard": [
                                                    [
                                                        { text: 'JavaScript', callback_data: 'JavaScript' },
                                                        { text: 'Java', callback_data: 'Java' },
                                                        { text: 'C#', callback_data: 'C_sharp' },
                                                        { text: 'Python', callback_data: 'Python' },
                                                        { text: 'Solidity', callback_data: 'Solidity' }
                                                    ]
                                                ],
                                                "hide_keyboard": true
                                            }
                                        });
                                    }
                                });
                            }
                        });
                    });
                }

                // if (db) {
                //     db.collection('bot_users', function(err, users_collection) {
                //         users_collection.findOne({
                //             'telegram_id': msg.from.id
                //         }, function(err, item) {
                //             if (item) {
                //                 console.log(item);
                //                 users_collection.update({
                //                     'telegram_id': msg.from.id
                //                 }, { '$set': { 'chat_id': msg.from.id, 'first_name': msg.from.first_name, 'last_name': msg.from.last_name, 'username': msg.from.username, 'invite_code': msg.text } }, {
                //                     safe: true
                //                 }, function(err, result) {
                //                     if (err) {
                //                         console.log("Error while saving in the DB", err);
                //                     } else {
                //                         console.log("successfully saved invite code in the db");
                //                     }
                //                 });
                //             }
                //         });
                //     });
                // }
            }
        }
    }
})

bot.onText(/\/start/, (msg) => {
    current_user_info[msg.from.id] = {};
    console.log(current_user_info);
    // console.log(msg);
    bot.sendMessage(msg.chat.id, "Welcome to the Indorse Bot!!! \n\nYou can start by <b>signing up</b> or else you can <b>validate your skill</b> by creating a claim and earn up to 50 Indorse tokens.\n\nTo get started, please select one of the options below... \nYou can always start over by clicking here - /start", {
        "reply_markup": {
            "keyboard": [
                ["Create a claim"],
                ["Sign Up"]
            ]
        },
        parse_mode: "HTML"
    });
    if (db) {
        db.collection('bot_users', function(err, users_collection) {
            users_collection.findOne({
                'telegram_id': msg.from.id
            }, function(err, item) {
                if (item) {
                    console.log(item);
                    users_collection.update({
                        'telegram_id': msg.from.id
                    }, { '$set': { 'chat_id': msg.from.id, 'first_name': msg.from.first_name, 'last_name': msg.from.last_name, 'username': msg.from.username } }, {
                        safe: true
                    }, function(err, result) {
                        if (err) {
                            console.log("Error while saving in the DB", err);
                        }
                    });
                } else {
                    //User is not in the DB. So just insert the data for user and update their email id
                    var user = {};
                    user['telegram_id'] = msg.from.id;
                    user['chat_id'] = msg.from.id;
                    user['first_name'] = msg.from.first_name;
                    user['last_name'] = msg.from.last_name;
                    user['username'] = msg.from.username;
                    users_collection.insert(user, {
                        safe: true
                    }, function(err, result) {
                        if (err) {
                            console.log("Error while saving the DB", err);
                        } else {
                            console.log("successfully saved invite code in the DB");
                        }
                    });
                }
            });
        });
    }
});

bot.on('callback_query', (msg) => {
    console.log("Inside callback_query...");
    // console.log(msg);
    // { text: 'JavaScript', callback_data: 'JavaScript' }, { text: 'Java', callback_data: 'Java' }, { text: 'C#', callback_data: 'C_sharp' }, { text: 'Python', callback_data: 'Python' }, { text: 'Solidity', callback_data: 'Solidity' },
    if (msg.data == "JavaScript") {
        bot.answerCallbackQuery(msg.id, "Next step - Add description", false);
        current_user_info[msg.from.id].selected_skill = "JavaScript";
        current_user_info[msg.from.id].status = "adding_description";
        console.log(current_user_info);
        bot.sendMessage(msg.message.chat.id, "Cool! Now it's time to prove it ;)\n\n<b>Let's get started with the claims process then!</b>\n\nFirst, <b>enter a description</b> for your claim. (Describe in detail what you can do and what you've done in this project). \nYou can always start over by clicking here - /start", {
            "reply_markup": {
                "hide_keyboard": true
            },
            parse_mode: "HTML"
        });
    } else if (msg.data == "Java") {
        bot.sendMessage(msg.message.chat.id, "Great! Please click on this link to <b>verify the claim</b> - https://indorse.io/validate.\n\nYou can always start over by clicking here - /start", {
            "reply_markup": {
                "keyboard": [
                    ["Finish"],
                ]
            },
            parse_mode: "HTML",
            "one_time_keyboard": true
        });
    } else if (msg.data == "C_sharp") {
        bot.sendMessage(msg.message.chat.id, "Great! Please click on this link to <b>verify the claim</b> - https://indorse.io/validate.\n\nYou can always start over by clicking here - /start", {
            "reply_markup": {
                "keyboard": [
                    ["Finish"],
                ]
            },
            parse_mode: "HTML",
            "one_time_keyboard": true
        });
    } else if (msg.data == "Python") {
        bot.answerCallbackQuery(msg.id, "Next step - Add description", false);
        current_user_info[msg.from.id].selected_skill = "Python";
        current_user_info[msg.from.id].status = "adding_description";
        console.log(current_user_info);
        bot.sendMessage(msg.message.chat.id, "Cool! Now it's time to prove it ;)\n\n<b>Let's get started with the claims process then!</b>\n\nFirst, <b>enter a description</b> for your claim. (Describe in detail what you can do and what you've done in this project). \nYou can always start over by clicking here - /start", {
            "reply_markup": {
                "hide_keyboard": true
            },
            parse_mode: "HTML"
        });
    } else if (msg.data == "Solidity") {
        bot.sendMessage(msg.message.chat.id, "Great! Please click on this link to <b>verify the claim</b> - https://indorse.io/validate.\n\nYou can always start over by clicking here - /start", {
            "reply_markup": {
                "keyboard": [
                    ["Finish"],
                ]
            },
            parse_mode: "HTML",
            "one_time_keyboard": true
        });
    } else if (msg.data == "invite_code_callback") {
        bot.answerCallbackQuery(msg.id, "Please enter your invite code", false);
        bot.sendMessage(msg.message.chat.id, "Please enter your invite code", {
            "reply_markup": {
                "hide_keyboard": true
            },
            parse_mode: "HTML"
        });
        current_user_info[msg.from.id].status = "adding_invite_code";
        console.log(current_user_info);
    } else if (msg.data == "no_invite_code_callback") {
        bot.answerCallbackQuery(msg.id, "Never mind, let's create a claim now!", false);
        bot.sendMessage(msg.message.chat.id, "You can validate your claim and earn up to 50 Indorse tokens.\nPlease select one of the skills below. \nYou can always start over by clicking here - /start", {
            "reply_markup": {
                "inline_keyboard": [
                    [
                        { text: 'JavaScript', callback_data: 'JavaScript' },
                        { text: 'Java', callback_data: 'Java' },
                        { text: 'C#', callback_data: 'C_sharp' },
                        { text: 'Python', callback_data: 'Python' },
                        { text: 'Solidity', callback_data: 'Solidity' },
                    ]
                ],
                "hide_keyboard": true
            }
        });
    } else if (msg.data == "beginner_level") {
        current_user_info[msg.from.id].level = "beginner";
        current_user_info[msg.from.id].status = "adding_link";
        console.log(current_user_info);
        bot.sendMessage(msg.message.chat.id, "Now you need to <b>add a proof</b> (could be a Github or a Bitbucket repo). \nYou can always start over by clicking here - /start", { parse_mode: "HTML" });
    } else if (msg.data == "intermediate_level") {
        current_user_info[msg.from.id].level = "intermediate";
        current_user_info[msg.from.id].status = "adding_link";
        console.log(current_user_info);
        bot.sendMessage(msg.message.chat.id, "Now you need to <b>add a proof</b> (could be a Github or a Bitbucket repo). \nYou can always start over by clicking here - /start", { parse_mode: "HTML" });
    } else if (msg.data == "expert_level") {
        current_user_info[msg.from.id].level = "expert";
        current_user_info[msg.from.id].status = "adding_link";
        console.log(current_user_info);
        bot.sendMessage(msg.message.chat.id, "Now you need to <b>add a proof</b> (could be a Github or a Bitbucket repo). \nYou can always start over by clicking here - /start", { parse_mode: "HTML" });
    }
});


// ========= The hell of commented code ========
// else if (msg.data == "description") {
//         // console.log("In the description callback");
//         // bot.answerCallbackQuery(msg.id, "Next step - Add a link", false);
//         // if (current_user_info[msg.from.id].description) {
//         //     current_user_info[msg.from.id].status = "adding_link";
//         //     console.log(current_user_info);
//         //     bot.sendMessage(msg.message.chat.id, "<b>Now you need to add a proof (could be a Github or a Bitbucket repo)</b>", { parse_mode: "HTML" });
//         // } else {
//         //     bot.sendMessage(msg.message.chat.id, "You first need to add some description");
//         // }
//     } else if (msg.data == "signup_adding_username") {
//         // console.log("In the signup username callback");
//         // current_signup_info[msg.from.id].status = "signup_adding_name";
//         // console.log(current_signup_info);
//         // bot.sendMessage(msg.message.chat.id, "<b>Now tell me your name</b>", { parse_mode: "HTML" });
//     } else if (msg.data == "signup_adding_fullname") {
//         // console.log("In the signup name callback");
//         // current_signup_info[msg.from.id].status = "signup_adding_email";
//         // console.log(current_signup_info);
//         // bot.sendMessage(msg.message.chat.id, "<b>Now tell me your email</b>", { parse_mode: "HTML" });
//     } else if (msg.data == "signup_adding_email") {
//         // console.log("In the signup email callback");
//         // console.log(current_signup_info);

//         // var request_url = API_URL + '/signup';
//         // var options = {
//         //     method: 'POST',
//         //     url: request_url,
//         //     headers: {
//         //         'cache-control': 'no-cache',
//         //         'content-type': 'application/json'
//         //     },
//         //     body: {
//         //         username: current_signup_info[msg.from.id].signup_username,
//         //         name: current_signup_info[msg.from.id].signup_name,
//         //         email: current_signup_info[msg.from.id].signup_email,
//         //         password: 'abcd1234'
//         //     },
//         //     json: true
//         // };

//         // request(options, function(error, response, body) {
//         //     if (error) {
//         //         console.log("Got an error from API - - - -");
//         //         console.log(error);
//         //     } else {
//         //         console.log("Got back a response from the API - - - - - ");
//         //         console.log(response.body);
//         //         if (response.body.message == "User with this email or username exists") {
//         //             bot.sendMessage(msg.message.chat.id, "It appears that a user with this email already exists. Please enter an email again.");
//         //         } else if (response.body.message == "Verification email sent successfully") {
//         //             bot.sendMessage(msg.message.chat.id, "Great! We have sent you an email with verification link and further instructions!\nWelcome to Indorse!\n\n<b>IMPORTANT - We have set up a temporary password for your email. You need to click 'Forgot Password' before logging in first time</b>", { parse_mode: "HTML" });
//         //         }
//         //     }
//         // });
//     } else if (msg.data == "link") {
//         // console.log("In the link callback");
//         // bot.answerCallbackQuery(msg.id, "Next step - Add email", false);
//         // if (current_user_info[msg.from.id].repo_link) {
//         //     current_user_info[msg.from.id].status = "adding_email"
//         //     bot.sendMessage(msg.message.chat.id, "<b>Now give us your email</b>", { parse_mode: "HTML" });
//         // } else {
//         //     bot.sendMessage(msg.message.chat.id, "You first need to add a link to a repo");
//         // }
//     } else if (msg.data == "email") {
//         // if (current_user_info[msg.from.id].claim_email) {
//         //     current_user_info[msg.from.id].status = "verifying_email"
//         //     bot.sendMessage(msg.message.chat.id, "Great! We have sent you a link on your email to verify the claim.", {
//         //         "reply_markup": {
//         //             "keyboard": [
//         //                 ["Finish"],
//         //             ]
//         //         },
//         //         "one_time_keyboard": true
//         //     });
//         //     var request_url = API_URL + '/graphql';
//         //     console.log(request_url);
//         //     var options = {
//         //         method: 'POST',
//         //         url: request_url,
//         //         headers: {
//         //             'cache-control': 'no-cache',
//         //             'content-type': 'application/json'
//         //         },
//         //         body: {
//         //             operationName: 'mutateCreateClaim',
//         //             variables: {
//         //                 email: current_user_info[msg.from.id].claim_email,
//         //                 title: 'Javascript',
//         //                 desc: current_user_info[msg.from.id].description,
//         //                 proof: current_user_info[msg.from.id].repo_link,
//         //                 level: 'beginner'
//         //             },
//         //             query: 'mutation mutateCreateClaim($email: EmailAddress!, $title: String!, $desc: String!, $proof: URL, $level: ClaimLevel!) {\n  createClaimDraft(email: $email, title: $title, desc: $desc, proof: $proof, level: $level) {\n    userExists\n    __typename\n  }\n}\n'
//         //         },
//         //         json: true
//         //     };

//         //     request(options, function(error, response, body) {
//         //         if (error) {
//         //             console.log(error);
//         //             bot.sendMessage(msg.message.chat.id, "There was some problem with your information!\n\nYou need to start over again!", {
//         //                 "reply_markup": {
//         //                     "keyboard": [
//         //                         ["/Start"],
//         //                     ]
//         //                 },
//         //                 "one_time_keyboard": true
//         //             });
//         //         } else {
//         //             current_user_info[msg.from.id] = {};
//         //         }
//         //         console.log(body);
//         //     });
//         // } else {
//         //     bot.sendMessage(msg.chat.id, "You first need to add an email");
//         // }
//     }
//     // bot.sendMessage(msg.chat.id, "You can validate your claim and stand a chance to earn 50 Indorse tokens.\nPlease select one of the skills below. \nYou can always start over by clicking here - /start", {
//     "reply_markup": {
//         "inline_keyboard": [
//             [
//                 { text: 'JavaScript', callback_data: 'JavaScript' },
//             ]
//         ],
//         "hide_keyboard": true
//     }
// });
// ==========================